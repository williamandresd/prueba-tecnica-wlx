package conduit.enums;

import lombok.Getter;

public enum EnumCreateUser {
    NULL;

    public enum DataTypeCreateUser {
        EXISTING_EMAIL("existingMail"),
        NEW_EMAIL("newEmail"),
        INVALID_EMAIL("invalidEmail"),
        NEW_USER_NAME("newUserName"),
        EXISTING_NAME("existingName"),
        EMPTY_PASSWORD("emptyPassword"),
        VALID_PASSWORD("passwordValid"),
        NULL("null");

        @Getter
        private String dataTypeCreateUser;

        DataTypeCreateUser(String dataTypeCreateUser) {
            this.dataTypeCreateUser = dataTypeCreateUser;
        }

    }

    public enum MessageResponse {
        IS_ALREADY_TAKEN("is already taken."),
        CANT_BE_BLANK("can't be blank"),
        PASSWORD_UNDEFINED("The \"password\" argument must be of type string or an instance of Buffer, TypedArray, or DataView. Received null");

        @Getter
        private String messageResponse;

        MessageResponse(String messageResponse) {
            this.messageResponse = messageResponse;
        }
    }

}

