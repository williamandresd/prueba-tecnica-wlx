package conduit.assertion;

import conduit.enums.EnumCreateUser;
import conduit.modelrequest.createuser.CreateUserRequest;
import conduit.modelresponse.createuser.CreateUserResponse;
import org.apache.http.HttpStatus;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class AssertionsCreateUser {

    private final String IS_ALREADY_TAKEN = EnumCreateUser.MessageResponse.IS_ALREADY_TAKEN.getMessageResponse();
    private final String CANT_BE_BLANK = EnumCreateUser.MessageResponse.CANT_BE_BLANK.getMessageResponse();
    private final String PASSWORD_UNDEFINED = EnumCreateUser.MessageResponse.PASSWORD_UNDEFINED.getMessageResponse();
    private final CreateUserRequest userData;
    private final CreateUserResponse userResponse;
    private final int statusCode;
    private final String emailType;
    private final String userNameType;
    private final String passwordType;

    public AssertionsCreateUser(CreateUserRequest userData, CreateUserResponse userResponse, int statusCode, String emailType, String userNameType, String passwordType) {
        this.userData = userData;
        this.userResponse = userResponse;
        this.statusCode = statusCode;
        this.emailType = emailType;
        this.userNameType = userNameType;
        this.passwordType = passwordType;
    }

    public void existingFiels() {
        if (statusCode == HttpStatus.SC_OK) {
            validCredentials();
        } else if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
            assertEmail();
            assertName();
        } else if (statusCode == HttpStatus.SC_INTERNAL_SERVER_ERROR) {
            assertPassword();
        }else {
            assertThat("No ingresó a ningun Assert",false);
        }
    }

    private void assertEmail() {
        if (EnumCreateUser.DataTypeCreateUser.EXISTING_EMAIL.getDataTypeCreateUser().equals(emailType)) {
            validWithexistingEmail();
        } else if (EnumCreateUser.DataTypeCreateUser.NEW_EMAIL.getDataTypeCreateUser().equals(emailType)) {
            
        } else if (EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser().equals(emailType)) {
            validWithEmailNull();
        }else {
            assertThat("No ingresó a ningun Assert",false);
        }
    }


    private void assertName() {
        if (EnumCreateUser.DataTypeCreateUser.EXISTING_NAME.getDataTypeCreateUser().equals(userNameType)) {
            validWithexistingUser();
        } else if (EnumCreateUser.DataTypeCreateUser.NEW_USER_NAME.getDataTypeCreateUser().equals(userNameType)) {
            
        } else if (EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser().equals(userNameType)) {
            validWithNameNull();
        }else {
            assertThat("No ingresó a ningun Assert",false);
        }
    }


    private void assertPassword() {
        if (EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser().equals(passwordType)) {
            assertThat(userResponse.getUser(), nullValue());
            assertThat(userResponse.getErrors().getMessage(), equalTo(PASSWORD_UNDEFINED));
        }
    }

    private void validCredentials() {
        assertThat(userData.getUser().getUsername(), equalToIgnoringCase(userResponse.getUser().getUsername()));
        assertThat(userData.getUser().getEmail(), equalTo(userResponse.getUser().getEmail()));
        assertThat(userResponse.getUser().getToken(), notNullValue());
    }

    private void validWithexistingEmail() {
        assertThat(userResponse.getUser(), nullValue());
        assertThat(userResponse.getErrors().getEmail(), equalTo(IS_ALREADY_TAKEN));
    }

    private void validWithexistingUser() {
        assertThat(userResponse.getUser(), nullValue());
        assertThat(userResponse.getErrors().getUsername(), equalTo(IS_ALREADY_TAKEN));
    }

    private void validWithEmailNull() {
        assertThat(userResponse.getUser(), nullValue());
        assertThat(userResponse.getErrors().getEmail(), equalTo(CANT_BE_BLANK));
    }

    private void validWithNameNull() {
        assertThat(userResponse.getUser(), nullValue());
        assertThat(userResponse.getErrors().getUsername(), equalTo(CANT_BE_BLANK));
    }


}
