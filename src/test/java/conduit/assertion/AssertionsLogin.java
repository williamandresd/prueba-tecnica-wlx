package conduit.assertion;

import conduit.modelrequest.login.LoginRequest;
import conduit.modelresponse.login.LoginResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class AssertionsLogin {
    private final LoginRequest userData;
    private final LoginResponse userResponse;

    public AssertionsLogin(LoginRequest userData, LoginResponse userResponse) {
        this.userData= userData;
        this.userResponse=userResponse;
    }

    public void loginSuccess(){
        assertThat(userData.getUser().getEmail(),equalToIgnoringCase(userResponse.getUser().getEmail()));
        assertThat(userResponse.getUser().getUsername(),notNullValue());
        assertThat(userResponse.getUser().getToken(),notNullValue());
    }

    public void failLogin() {
        //// TODO: 7/24/2022  
    }
}
