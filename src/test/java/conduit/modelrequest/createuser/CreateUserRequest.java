package conduit.modelrequest.createuser;

import lombok.Data;

@Data
public class CreateUserRequest{
	private User user;
}