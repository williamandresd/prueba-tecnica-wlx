package conduit.modelrequest.login;

import lombok.Data;

@Data
public class LoginRequest{
	private User user;
}