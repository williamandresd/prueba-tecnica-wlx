package conduit.modelrequest.login;

import lombok.Data;

@Data
public class User{
	private String password;
	private String email;
}