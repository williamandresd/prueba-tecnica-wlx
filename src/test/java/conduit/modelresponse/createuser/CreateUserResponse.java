package conduit.modelresponse.createuser;

import lombok.Data;

@Data
public class CreateUserResponse{
	private User user;
	private Errors errors;

	@Data
	public static class Error{

	}
}