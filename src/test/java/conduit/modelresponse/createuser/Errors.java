package conduit.modelresponse.createuser;

import lombok.Data;

@Data
public class Errors{
	private String email;
	private String username;
	private String message;
	private CreateUserResponse.Error error;
}