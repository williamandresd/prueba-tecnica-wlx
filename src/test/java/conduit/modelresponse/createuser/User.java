package conduit.modelresponse.createuser;

import lombok.Data;

@Data
public class User{
	private String email;
	private String username;
	private String token;
	//private String bio;
	//private String image;
}