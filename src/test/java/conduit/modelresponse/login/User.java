package conduit.modelresponse.login;

import lombok.Data;

@Data
public class User{
	private String image;
	private String bio;
	private String email;
	private String token;
	private String username;
}