package conduit.modelresponse.login;

import lombok.Data;

@Data
public class LoginResponse {
	private User user;
	private Errors errors;
}