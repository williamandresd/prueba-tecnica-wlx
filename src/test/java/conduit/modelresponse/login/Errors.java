package conduit.modelresponse.login;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Errors{
	private String password;
	private String email;
	@JsonProperty("email or password")
	private String emailOrPassword;
}