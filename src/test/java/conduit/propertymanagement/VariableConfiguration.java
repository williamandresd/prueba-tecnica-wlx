package conduit.propertymanagement;

import java.util.Optional;

public class VariableConfiguration {
    public static String getBaseuri(){
        return Optional.ofNullable(System.getenv("baseuri"))
                .orElse(ApplicationProperties.getInstance().get("baseuri").toString());
    }
    public static String getBasepath(){
        return Optional.ofNullable(System.getenv("basepath"))
                .orElse(ApplicationProperties.getInstance().get("basepath").toString());
    }
}
