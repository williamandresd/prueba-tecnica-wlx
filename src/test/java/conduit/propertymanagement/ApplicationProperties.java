package conduit.propertymanagement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

import static org.apache.logging.log4j.core.util.Loader.getClassLoader;

public class ApplicationProperties {
    private static final String APPLICATION_PREFIX = "application";
    private static final Object APPLICATION_SUFFIX = "properties";

    public static final Logger logger = LogManager.getLogger(ApplicationProperties.class);
    private static Properties instance = null;

    public static Properties getInstance() {
        if (instance == null) {
            instance = loadProperties();
        }
        return instance;
    }

    private static Properties loadProperties() {
        String environment = Optional.ofNullable(System.getenv("env")).orElse("dev");
        String fileName = String.format("%s-%s.%s", APPLICATION_PREFIX, environment, APPLICATION_SUFFIX);
        logger.info("Property file to read {}", fileName);

        Properties prop = new Properties();
        try {
            prop.load(getClassLoader().getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("unable to load the file {}", fileName);
            throw new RuntimeException(e);
        }
        return prop;
    }
}
