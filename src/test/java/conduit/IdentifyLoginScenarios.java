package conduit;

import conduit.enums.EnumCreateUser;
import conduit.factory.LoginDataFactory;
import conduit.modelrequest.login.LoginRequest;

public class IdentifyLoginScenarios {
    private final String emailType;
    private final String passwordType;

    public IdentifyLoginScenarios() {
        this.emailType = EnumCreateUser.DataTypeCreateUser.EXISTING_EMAIL.getDataTypeCreateUser();
        this.passwordType = EnumCreateUser.DataTypeCreateUser.VALID_PASSWORD.getDataTypeCreateUser();
    }

    public IdentifyLoginScenarios(String emailType, String passwordType) {
        this.emailType = emailType;
        this.passwordType = passwordType;
    }

    public LoginRequest scenarioRecognition() {

        LoginDataFactory loginDataFactory = LoginDataFactory.anUser();

        if (emailType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.INVALID_EMAIL.getDataTypeCreateUser())) {
            loginDataFactory.loginWithInvalidEmail();
        } else if (emailType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.EXISTING_EMAIL.getDataTypeCreateUser())) {
            System.out.println("");
        } else if (emailType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser())) {
            loginDataFactory.loginWithNullEmail();
        } else {
            return null;
        }

        if (passwordType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.EMPTY_PASSWORD.getDataTypeCreateUser())) {
            loginDataFactory.loginWithEmptyPassword();
        } else if (passwordType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser())) {
            loginDataFactory.loginWithNullPassword();
        } else if (passwordType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.VALID_PASSWORD.getDataTypeCreateUser())) {
            System.out.println("");
        } else {
            return null;
        }
        return loginDataFactory.build();
    }

}
