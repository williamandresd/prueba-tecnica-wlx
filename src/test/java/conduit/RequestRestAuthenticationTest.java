package conduit;

import conduit.assertion.AssertionsCreateUser;
import conduit.assertion.AssertionsLogin;
import conduit.modelrequest.createuser.CreateUserRequest;
import conduit.modelrequest.login.LoginRequest;
import conduit.modelresponse.createuser.CreateUserResponse;
import conduit.modelresponse.login.LoginResponse;
import conduit.propertymanagement.VariableConfiguration;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static io.restassured.RestAssured.given;

public class RequestRestAuthenticationTest {

    public static final Logger logger = LogManager.getLogger(RequestRestAuthenticationTest.class);

    @BeforeEach
    public void setup() {
        logger.info("Iniciando el robot");
        RestAssured.baseURI = "https://angular-conduit-node.herokuapp.com";
        RestAssured.basePath = "/api";
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());

    }

    @AfterEach
    public void end() {
        logger.info("Fin de la ejecución");
    }

    private RequestSpecification requestSpecification() {
        return new RequestSpecBuilder()
                .setBaseUri(VariableConfiguration.getBaseuri())
                .setBasePath(VariableConfiguration.getBasepath())
                .setContentType(ContentType.JSON)
                .build();
    }

    private ResponseSpecification responseSpecification(String httpsStatus) {
        return new ResponseSpecBuilder()
                .expectStatusCode(Integer.parseInt(httpsStatus))
                .expectContentType(ContentType.JSON)
                .build();
    }

    @ParameterizedTest(name = "Escenario: {0}")
    @CsvSource({
            //Scenario Name:                Email:          Password:       UserName:       httpsStatus:
            "Credenciales validas,          newEmail,       passwordValid,  newUserName,    201",
            "Credenciales validas,          newEmail,       passwordValid,  newUserName,    200",

            "Correo existente,              existingMail,   passwordValid,  newUserName,    422",
            "Correo y nombre existente,     existingMail,   passwordValid,  existingName,   422",
            "Nombre existente,              newEmail,        passwordValid,  existingName,   422",

            "Correo NULL,                   null,           passwordValid,  newUserName,    422",
            "Contrasena NULL,               newEmail,       null,           newUserName,    422",
            "Nombre NULL,                   newEmail,       passwordValid,  null,           422"

            //TODO IMPLEMENTAR
//            "Correo mal estructurado,       invalidEmail,   passwordValid,  newUserName,    #",
//            "Correo con espacios,           emailWithSpace, passwordValid,  newUserName,    #",
//            "Contraseña vacia,              newMail,        emptyPassword,  newUserName,    #",
//            "Usuario vacio,                 newMail,        passwordValid,  emptyUserName,  #",
//            "Usuario con espacios,          newMail,        passwordValid,  userWithSpace,  #",
//            "Usuario > 50 caracteres,       newMail,        passwordValid,  userGreater,    #",

    })
    public void CreateNewUser(String s, String emailType, String passwordType, String userNameType, String httpsStatus) {

        logger.info("Escenario: {}", s);

        IdentifyCreateUserScenario identifyCreateUserScenario = new IdentifyCreateUserScenario(emailType, passwordType, userNameType);
        CreateUserRequest userData = identifyCreateUserScenario.scenarioRecognition();

        Response response = given()
                .spec(requestSpecification())
                .when()
                .body(userData)
                .post("users");

        CreateUserResponse userResponse = response.then()
                .spec(responseSpecification(httpsStatus))
                .extract()
                .body().as(CreateUserResponse.class);

        AssertionsCreateUser assertionsCreateUser = new AssertionsCreateUser(userData, userResponse, response.getStatusCode(), emailType, userNameType, passwordType);
        assertionsCreateUser.existingFiels();
    }

    //region LOGIN
    @Test
    public void login() {
        IdentifyLoginScenarios identifyLoginScenario = new IdentifyLoginScenarios();
        LoginRequest userData = identifyLoginScenario.scenarioRecognition();

        LoginResponse userResponse = given()
                .spec(requestSpecification())
                .when()
                .body(userData)
                .post("users/login")
                .then()
                .spec(responseSpecification("200"))
                .extract()
                .body().as(LoginResponse.class);


        AssertionsLogin assertionsLogin = new AssertionsLogin(userData, userResponse);
        assertionsLogin.loginSuccess();
    }

    @ParameterizedTest(name = "Escenario: {0}")
    @CsvSource({
            //Scenario Name:    Email,          password        httpsStatus:
            "Correo invalido,   invalidEmail,   passwordValid,  422",
            "Correo null,       null,           passwordValid,  422",
            "Contrasena vacia,  existingMail,   emptyPassword,  422",
            "Contrasena null,   existingMail,   null,           422"
    })
    public void loginFailed(String scenario, String emailType, String passwordType, String httpsStatus) {

        IdentifyLoginScenarios identifyLoginScenario = new IdentifyLoginScenarios(emailType, passwordType);
        LoginRequest userData = identifyLoginScenario.scenarioRecognition();


        LoginResponse userResponse = given()
                .spec(requestSpecification())
                .when()
                .body(userData)
                .post("users/login")
                .then()
                .spec(responseSpecification(httpsStatus))
                .extract()
                .body().as(LoginResponse.class);


        AssertionsLogin assertionsLogin = new AssertionsLogin(userData, userResponse);
        assertionsLogin.failLogin();


    }

    //endregion
}
