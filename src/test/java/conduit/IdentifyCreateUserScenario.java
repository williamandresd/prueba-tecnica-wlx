package conduit;

import conduit.enums.EnumCreateUser;
import conduit.factory.CreateUserDataFactory;
import conduit.modelrequest.createuser.CreateUserRequest;

public class IdentifyCreateUserScenario {

    private String emailType;
    private String passwordType;
    private String userNameType;

    public IdentifyCreateUserScenario(String emailType, String passwordType, String userNameType) {
        this.emailType = emailType;
        this.passwordType = passwordType;
        this.userNameType = userNameType;
    }

    public CreateUserRequest scenarioRecognition(){
        CreateUserDataFactory createUserDataFactory = CreateUserDataFactory.anUser();

        if(emailType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.EXISTING_EMAIL.getDataTypeCreateUser())){
            createUserDataFactory.createUserWithexistingEmail();

        } else if (emailType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser())) {
            createUserDataFactory.createUserWithNullEmail();
        }

        if(userNameType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.EXISTING_NAME.getDataTypeCreateUser())){
            createUserDataFactory.createUserWithexistingName();
        }else if(userNameType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser())){
            createUserDataFactory.createUserWithNullName();
        }

        if(passwordType.equalsIgnoreCase(EnumCreateUser.DataTypeCreateUser.NULL.getDataTypeCreateUser())){
            createUserDataFactory.createUserWithNullPassword();
        }

        return createUserDataFactory.build();
    }

}
