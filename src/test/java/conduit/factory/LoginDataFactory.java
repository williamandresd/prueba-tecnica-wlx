package conduit.factory;

import conduit.modelrequest.login.LoginRequest;
import conduit.modelrequest.login.User;
import conduit.propertymanagement.ApplicationProperties;

public class LoginDataFactory {

    private static final String EXISTING_EMAIL = ApplicationProperties.getInstance().getProperty("user.email.valid");
    private static final String INVALID_EMAIL = ApplicationProperties.getInstance().getProperty("user.email.invalid");
    private static final String CORRECT_PASSWORD = ApplicationProperties.getInstance().getProperty("user.password.valid");
    private String email;
    private String password;
    private User user;
    private LoginRequest loginRequest;

    public static LoginDataFactory anUser() {
        return new LoginDataFactory();
    }

    public LoginDataFactory() {
        defaultLogin();
    }

    private void defaultLogin() {
        this.email=EXISTING_EMAIL;
        this.password=CORRECT_PASSWORD;
    }

    public void loginWithInvalidEmail(){
        this.email=INVALID_EMAIL;
    }
    public void loginWithNullEmail(){
        this.email=null;
    }
    public void loginWithEmptyPassword(){
        this.password="";
    }
    public void loginWithNullPassword(){
        this.password=null;
    }

    public LoginRequest build(){
        loginRequest = new LoginRequest();
        user = new User();

        user.setEmail(email);
        user.setPassword(password);
        loginRequest.setUser(user);

        return loginRequest;
    }

}
