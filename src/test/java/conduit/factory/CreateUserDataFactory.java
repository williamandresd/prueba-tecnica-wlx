package conduit.factory;

import com.github.javafaker.Faker;
import conduit.modelrequest.createuser.CreateUserRequest;
import conduit.modelrequest.createuser.User;
import conduit.propertymanagement.ApplicationProperties;

public class CreateUserDataFactory {

    public static final Faker faker = new Faker();
    public static final String EXISTING_EMAIL = ApplicationProperties.getInstance().getProperty("user.email.valid");
    private static final String EXISTING_USERNAME = ApplicationProperties.getInstance().getProperty("user.name.valid");
    public static final String DEFAULT_PASSWORD = ApplicationProperties.getInstance().getProperty("user.password.valid");

    private String email;
    private String password;
    private String userName;

    CreateUserRequest createUserRequest;

    private CreateUserDataFactory() {
        defaultUser();
    }

    public static CreateUserDataFactory anUser() {
        return new CreateUserDataFactory();
    }

    private void defaultUser() {
        this.email = String.format(System.currentTimeMillis() + faker.internet().emailAddress());
        this.password = DEFAULT_PASSWORD;
        this.userName = String.format(faker.name().firstName() + System.currentTimeMillis());
    }


    public CreateUserDataFactory createUserWithexistingEmail() {
        this.email = EXISTING_EMAIL;
        return this;
    }

    public void createUserWithexistingMailAndName() {
        this.email = EXISTING_EMAIL;
        this.userName = EXISTING_USERNAME;

    }

    public CreateUserDataFactory createUserWithexistingName() {
        this.userName = EXISTING_USERNAME;
        return this;
    }

    public CreateUserDataFactory createUserWithNullEmail() {
        this.email = null;
        return this;
    }

    public CreateUserDataFactory createUserWithNullName() {
        this.userName = null;
        return this;
    }

    public CreateUserDataFactory createUserWithNullPassword() {
        this.password = null;
        return this;
    }

    public CreateUserRequest build() {
        createUserRequest = new CreateUserRequest();
        User user = new User();

        user.setEmail(email);
        user.setPassword(password);
        user.setUsername(userName);
        createUserRequest.setUser(user);

        return createUserRequest;
    }


}
